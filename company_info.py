import urllib2
import re
from bs4 import BeautifulSoup
import unittest


def get_soup (url):
    html_source = urllib2.urlopen(url).read()
    soup = BeautifulSoup(html_source)
    return soup

def get_first_table(soup):
    table = soup.findAll('table', {'class' : 'datatable'})[0]
    return (table.findAll('td'))

def get_details(soup):
    table = soup.findAll('table', {'class' : 'contenttable company-details'})[0]
    return (table.findAll('td'))

def get_issuer_code(rows):
    return rows[0].string

def get_official_listing_date(rows):
    return rows[2].string

def get_gics_industry_group(rows):
    return rows[3].string

def get_exempt_foreign (rows):
    return rows[4].string.strip()

def get_internet_address (rows):
    return rows[5].string

def get_registered_office_address (rows):
    return rows[6].string

def get_head_office_telephone (rows):
    return rows[7].string

def get_head_office_fax (rows):
    return rows[8].string

def get_share_registry (rows):
        row = str(rows[9])
        MATCH_START = '<td>'
        MATCH_BREAK = '<br>'
        MATCH_END = '</br></td>'
        break_point = row.find(MATCH_BREAK)
        name = row[len(MATCH_START):break_point]
        address = row[break_point + len(MATCH_BREAK):-len(MATCH_END)]
        return (name, address)

def get_share_registry_telephone (rows):
    return rows[10].string

def get_bid(rows):
    return '%.3f' % float(rows[2].string)

def get_offer(rows):
    return '%.3f' % float(rows[3].string)

def get_open(rows):
    return '%.3f' % float(rows[4].string)

def get_high(rows):
    return '%.3f' % float(rows[5].string)

def get_low(rows):
    return '%.3f' % float(rows[6].string)

def get_volume(rows):
    return int(rows[7].string.replace(',',''))

def get_last_price(rows):
    return '%.3f' % float(rows[0].string)

def get_pcent_change(rows):
    return float(re.findall("\d+.\d+", rows[1].string)[0])


class company_info (object):
    """loads various parameters about a share from the asx website"""

    def __init__ (self, code):
        BASE_URL = 'http://www.asx.com.au/asx/research/companyInfo.do?by=asxCode&asxCode='
        self.soup = get_soup(BASE_URL + code)
        self.code = code
        self.details = get_details(self.soup)
        self.first_table = get_first_table(self.soup)

    # first table
    def last (self):
        return get_last_price(self.first_table)

    def percent_change (self):
        return get_pcent_change(self.first_table)

    def bid (self):
        return get_bid(self.first_table)

    def offer (self):
        return get_offer(self.first_table)

    def open (self):
        return get_open(self.first_table)

    def high (self):
        return get_high(self.first_table)

    def low (self):
        return get_low(self.first_table)

    def volume (self):
        return get_volume(self.first_table)

    # details table
    def issuer_code (self):
        return get_issuer_code(self.details)

    def official_listing_date (self):
        return get_official_listing_date(self.details)

    def gics_industry_group (self):
        return get_gics_industry_group(self.details)

    def exempt_foreign (self):
        return get_exempt_foreign(self.details)

    def internet_address (self):
        return get_internet_address(self.details)

    def registered_office_address (self):
        return get_registered_office_address(self.details)

    def head_office_telephone (self):
        return get_head_office_telephone(self.details)

    def head_office_fax (self):
        return get_head_office_fax(self.details)

    def share_registry (self):
        return get_share_registry(self.details)

    def share_registry_telephone (self):
        return get_share_registry_telephone(self.details)


    # Directors / Senior Management table
    # announcements
    # closing prices table
    # devidend table

class test_bhp_company_info(unittest.TestCase):
    def setUp(self):
        with open ("bhp.company_info.html", "r") as f:
            source = f.read().replace('\n', '')
        self.soup = BeautifulSoup(source)
        self.details = get_details(self.soup)
        self.first_table = get_first_table(self.soup)

    def test_issuer_code(self):
        self.assertEqual(get_issuer_code(self.details), 'BHP')

    def test_official_listing_date(self):
        self.assertEqual(get_official_listing_date(self.details),'13 August, 1885')

    def test_gics_industry_group(self):
        self.assertEqual(get_gics_industry_group(self.details), 'Materials')

    def test_exempt_foreign(self):
        self.assertEqual(get_exempt_foreign(self.details), 'No')

    def test_internet_address(self):
        self.assertEqual(get_internet_address(self.details), 'http://www.bhpbilliton.com/')

    def test_registered_office_address(self):
        self.assertEqual(get_registered_office_address(self.details), 'Level 16, 171 Collins Street, MELBOURNE, VIC, AUSTRALIA, 3000')

    def test_head_office_telephone(self):
        self.assertEqual(get_head_office_telephone(self.details), '(61) 1300 55 47 57')

    def test_head_office_fax(self):
        self.assertEqual(get_head_office_fax(self.details), '(61 3) 9609 3015')

    def test_share_registry(self):
        self.assertEqual(get_share_registry(self.details), ('COMPUTERSHARE INVESTOR SERVICES PTY LIMITED', 'YARRA FALLS, 452 JOHNSTON STREET, ABBOTSFORD, VIC, AUSTRALIA, 3067'))

    def test_share_registry_telephone(self):
        self.assertEqual(get_share_registry_telephone(self.details), '1300 787 272')

bhp = company_info('bhp')
# first table
print bhp.last()
print bhp.percent_change()
print bhp.bid()
print bhp.offer()
print bhp.open()
print bhp.high()
print bhp.low()
print bhp.volume()

unittest.main()
