import urllib2
import re
from bs4 import BeautifulSoup
import unittest
import string

def get_soup (url):
    html_source = urllib2.urlopen(url).read()
    soup = BeautifulSoup(html_source)
    return soup

def get_first_table(soup):
    table = soup.findAll('table', {'class' : 'datatable'})[0]
    return (table.findAll('td'))

def get_details(soup):
    table = soup.findAll('table', {'class' : 'contenttable company-details'})[0]
    return (table.findAll('td'))

def get_issuer_code(rows):
    return rows[0].string

def get_official_listing_date(rows):
    return rows[2].string

def get_gics_industry_group(rows):
    return rows[3].string

def get_exempt_foreign (rows):
    return rows[4].string.strip()

def get_internet_address (rows):
    return rows[5].string

def get_registered_office_address (rows):
    return rows[6].string

def get_head_office_telephone (rows):
    return rows[7].string

def get_head_office_fax (rows):
    return rows[8].string

def get_share_registry (rows):
        row = str(rows[9])
        MATCH_START = '<td>'
        MATCH_BREAK = '<br>'
        MATCH_END = '</br></td>'
        break_point = row.find(MATCH_BREAK)
        name = row[len(MATCH_START):break_point]
        address = row[break_point + len(MATCH_BREAK):-len(MATCH_END)]
        return (name, address)

def get_share_registry_telephone (rows):
    return rows[10].string

def get_bid(rows):
    return '%.3f' % float(rows[2].string)

def get_offer(rows):
    return '%.3f' % float(rows[3].string)

def get_open(rows):
    return '%.3f' % float(rows[4].string)

def get_high(rows):
    return '%.3f' % float(rows[5].string)

def get_low(rows):
    return '%.3f' % float(rows[6].string)

def get_volume(rows):
    return int(rows[7].string.replace(',',''))

def get_last_price(rows):
    return '%.3f' % float(rows[0].string)

def get_pcent_change(rows):
    return float(re.findall("\d+.\d+", rows[1].string)[0])


class company_info (object):
    """loads various parameters about a share from the asx website"""

    def __init__ (self, code):
        BASE_URL = 'http://www.asx.com.au/asx/research/companyInfo.do?by=asxCode&asxCode='
        self.soup = get_soup(BASE_URL + code)
        self.code = code
        self.details = get_details(self.soup)
        self.first_table = get_first_table(self.soup)

    # first table
    def last (self):
        return get_last_price(self.first_table)

    def percent_change (self):
        return get_pcent_change(self.first_table)

    def bid (self):
        return get_bid(self.first_table)

    def offer (self):
        return get_offer(self.first_table)

    def open (self):
        return get_open(self.first_table)

    def high (self):
        return get_high(self.first_table)

    def low (self):
        return get_low(self.first_table)

    def volume (self):
        return get_volume(self.first_table)

    # details table
    def issuer_code (self):
        return get_issuer_code(self.details)

    def official_listing_date (self):
        return get_official_listing_date(self.details)

    def gics_industry_group (self):
        return get_gics_industry_group(self.details)

    def exempt_foreign (self):
        return get_exempt_foreign(self.details)

    def internet_address (self):
        return get_internet_address(self.details)

    def registered_office_address (self):
        return get_registered_office_address(self.details)

    def head_office_telephone (self):
        return get_head_office_telephone(self.details)

    def head_office_fax (self):
        return get_head_office_fax(self.details)

    def share_registry (self):
        return get_share_registry(self.details)

    def share_registry_telephone (self):
        return get_share_registry_telephone(self.details)


code_list_url = 'http://www.asx.com.au/asx/research/listedCompanies.do?coName='
code_prefix = string.ascii_uppercase

for l in code_prefix:
    soup = get_soup(code_list_url + l)
    codes = soup.findAll("a", {"href" : re.compile(r'companyInfo.do\?by=asxCode&asxCode=*')})
    for code in codes:
        try:
            info = company_info(code.text.strip())
            print '[' + info.code + '] Company Information'
            print 'Last Price: ' + str(info.last())
            print 'Bid Price: ' + str(info.bid())
            print 'Offer Price: ' + str(info.offer())
            print 'Open Price: ' + str(info.open())
            print 'High Price: ' + str(info.high())
            print 'Low Price: ' + str(info.low())
            print 'Volume: ' + str(info.volume())
            print 'Head Office Telephone: ' + str(info.head_office_telephone())
            print 'Address: ' + str(info.registered_office_address())
            print ''
        except:
            pass
